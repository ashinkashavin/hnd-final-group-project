<?php
include('authenticationseller.php');
?>
<?php
include_once 'Connection.php';
$result = mysqli_query($conn,"SELECT * FROM Vehicle");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Boxicons -->
	<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
	<!-- My CSS -->
	<link rel="stylesheet" href="admin.css">

	<title>SellerHub</title>
</head>
<body>


	<!-- SIDEBAR -->
	<section id="sidebar">
		<a href="#" class="brand">
			<i class='bx bxs-user'></i>
			<span class="text">SellerHub</span>
		</a>
		<ul class="side-menu top">
			<li>
				<a href="sellerpanel.php">
					<i class='bx bxs-dashboard' ></i>
					<span class="text">Dashboard</span>
				</a>
			</li>
			<li  class="active">
				<a href="Sproduct.php">
					<i class='bx bxl-product-hunt' ></i>
					<span class="text">Manage Products</span>
				</a>
			</li>
			<li>
				<a href="sellerbidding.php">
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">Manage Bidding</span>
				</a>
			</li>
		</ul>
		<ul class="side-menu">
				
			<li>
				<i class='bx bxs-log-out-circle' >
				<form action="Alogout.php" method="POST">
              <button type="submit" name="logout" class="dropdown-item">Logout</button>
</i>
            </form>	
			</li>
		</ul>
	</section>
	<!-- SIDEBAR -->



	<!-- CONTENT -->
	<section id="content">
		<!-- NAVBAR -->
		<nav>
			<i class='bx bx-menu' ></i>
			<a href="#" class="nav-link">Categories</a>
			<form method="post" action="searchimgnew.php" id="searchform">
				<div class="form-input">
					<input type="text" name="Brand_Name" id="Brand_Name" placeholder="Search...">
					<button input type="submit" name="submit" class="search-btn"><i class='bx bx-search' ></i></button>
				</div>
			</form>
			
		</nav>
		<!-- NAVBAR -->

		<!-- MAIN -->
		<main>
			<div class="head-title">
				<div class="left">
					<h1>Dashboard</h1>
					
				</div>
				
			</div>

			<!--<ul class="box-info">
				<li>
					<i class='bx bxs-calendar-check' ></i>
					<span class="text">
						<h3>1020</h3>
						<p>New Order</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-group' ></i>
					<span class="text">
						<h3>2834</h3>
						<p>Visitors</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">
						<h3>$2543</h3>
						<p>Total Sales</p>
					</span>
				</li>
			</ul>-->


			<div class="table-data">
				<div class="order">
					<div class="head">
						<h3>Recent Orders</h3>
						<i class='bx bx-search' ></i>
						<i class='bx bx-filter' ></i>
					</div>
					<?php
if(isset($_POST['submit']))
{ include "Connection.php";

  
  $Category = $_POST['Category'];
  $Price = $_POST['Price'];
  $Brand_Name = $_POST['Brand_Name'];
  $Model = $_POST['Model'];
  $Vehicle_Image = $_FILES['Vehicle_Image']['name'];
  $Milage = $_POST['Milage'];
  $User = $_POST['User'];
  $Transmission=$_POST['Transmission'];
  $FuelType=$_POST['FuelType'];

//print_r($_FILES);

 $target = "upload/".basename($Vehicle_Image);//The basename() function returns the filename from a path

$sql = "insert into vehicle
  (Category,Brand_Name,Model,Price,Milage,User,Transmission,FuelType,Vehicle_Image)
  values ('$Category','$Brand_Name','$Model','$Price','$Milage','$User','$Transmission','$FuelType', '$Vehicle_Image')";
	
	$results = mysqli_query($conn, $sql);           
            if(!$results)
			{
               die('Could not enter data: ' . mysqli_error($conn));
            }
			else
			{
				echo '<script type ="text/JavaScript">';  
echo 'alert("Entered data successfully\n")';  
echo '</script>';           
			}	
				//The move_uploaded_file() function can move an uploaded file to new location		
	  if (move_uploaded_file($_FILES['Vehicle_Image']['tmp_name'], $target)) {
		  echo '<script type ="text/JavaScript">';  
echo 'alert("Image uploaded successfully")';  
echo '</script>';
				 //print_r($_FILES);
   				}else{
		    echo '<script type ="text/JavaScript">';  
echo 'alert("Failed to upload image")';  
echo '</script>';
   					}
  		   }  
?>
<html>
	<head>
	<link rel="stylesheet" href="saveimg.css">
	
	</head>
<body>
	<div class="content">
  <div class="new">
<form action="" method="post" enctype="multipart/form-data"  name="form1" id="form1"><!--multipart/form-data(method of encoding for the form data) is used when a form requires binary data, like the contents of a file, to be uploaded .-->
	<h1>ADD Vehicle</h1>
  <table width="367" border="0" cellspacing="3" cellpadding="2">
	  <tr>
      <th scope="row">Brand Name</th>
      <td><label for="des"></label>
		 <input type="text" name="Brand Name" id="Brand Name" /></td>
    </tr>
   
    <tr>
      <th scope="row">Model</th>
      <td><label for="Model"></label>
      <input type="text" name="Model" id="Model" /></td>
    </tr>
	   <tr>
      <th scope="row">Category</th>
      <td><select name="Category" id="Category">
  <option value="Car">Car</option>
  <option value="Jet Ski">JetSki</option>
  <option value="Bike">Bike</option>
  <option value="Classic Vehicles">Classic</option>
</select>
     </td>
    </tr>
    <tr>
      <th scope="row">Price</th>
      <td><label for="des"></label>
		 <input type="text" name="Price" id="Price" /></td>
    </tr>
   
    <tr>
      <th scope="row">Milage</th>
      <td><label for="Milage"></label>
      <input type="text" name="Milage" id="Milage" /></td>
    </tr>
    <tr>
      <th scope="row">User</th>
      <td><label for="User"></label>
      <input type="text" name="User" id="User" /></td>
    </tr>
	  <tr>
      <th scope="row">Transmission</th>
      <td><label for="Transmission"></label>
      <input type="text" name="Transmission" id="Transmission" /></td>
    </tr>
	  
	   <tr>
      <th scope="row">FuelType</th>
      <td><label for="FuelType"></label>
      <input type="text" name="FuelType" id="FuelType" /></td>
    </tr>
	  <tr>
      <th scope="row">Vehicle Image</th>
      <td><p>
        <label for="img"></label>
        <label for="Bid_image"></label>
        <label for="img2"></label>
        <input type="hidden" name="size" value="1000000">
        <input type="file" name="Bid_image" id="img2" /><!---allowuser choose one or more files from their device storage. Once chosen, the files can be uploaded to a server -->
      </p></td>
    </tr>
    <tr>
      <th scope="row"><input type="submit" name="submit" id="submit" value="Submit" /></th>
      <td><input type="reset" name="cancle" id="cancle" value="Reset" /></td>
    </tr>
  </table>
  <p>&nbsp;</p>
</form>
	  </div>
	</div>
</body>
</html>
				</div>
				
		</main>
		<!-- MAIN -->
	</section>
	<!-- CONTENT -->
	

	<script src="script.js"></script>
</body>
</html>