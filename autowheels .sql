-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 09, 2023 at 03:52 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autowheels`
--

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

DROP TABLE IF EXISTS `bids`;
CREATE TABLE IF NOT EXISTS `bids` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `UserId` int(30) NOT NULL,
  `bidid` int(30) NOT NULL,
  `bid_amount` float NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=bid,2=confirmed,3=cancelled',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bids`
--

INSERT INTO `bids` (`id`, `UserId`, `bidid`, `bid_amount`, `status`, `date_created`) VALUES
(2, 5, 1, 7500, 1, '2020-10-27 14:18:50'),
(4, 5, 3, 155000, 1, '2020-10-27 16:37:29'),
(5, 1, 11, 1850000, 1, '2022-12-31 22:33:27'),
(6, 1, 14, 2100000, 1, '2023-01-01 12:34:45'),
(7, 1, 13, 2200000, 1, '2023-01-06 17:43:02'),
(8, 1, 14, 2200000, 1, '2023-01-06 17:43:58'),
(9, 1, 15, 2200000, 1, '2023-01-06 17:46:31'),
(10, 1, 12, 1900000, 1, '2023-01-06 17:46:57'),
(11, 1, 12, 1900000, 1, '2023-01-06 17:47:21'),
(12, 1, 11, 1950000, 1, '2023-01-06 17:47:33'),
(13, 1, 12, 1950000, 1, '2023-01-06 17:50:31'),
(14, 1, 11, 2000000, 1, '2023-01-06 17:55:20'),
(15, 1, 14, 2300000, 1, '2023-01-06 20:56:54'),
(16, 1, 12, 2000000, 1, '2023-01-06 21:53:01'),
(17, 1, 11, 2100000, 1, '2023-01-07 19:08:24'),
(18, 1, 15, 2250000, 1, '2023-01-07 23:25:42');

-- --------------------------------------------------------

--
-- Table structure for table `regbuyer`
--

DROP TABLE IF EXISTS `regbuyer`;
CREATE TABLE IF NOT EXISTS `regbuyer` (
  `UserId` int(10) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(10) NOT NULL,
  `password` varchar(15) NOT NULL,
  `UserType` varchar(20) NOT NULL,
  `secquestion` varchar(100) NOT NULL,
  `answer` varchar(100) NOT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regbuyer`
--

INSERT INTO `regbuyer` (`UserId`, `fname`, `lname`, `email`, `phone`, `password`, `UserType`, `secquestion`, `answer`) VALUES
(8, 'Lakshan', 'widyapathi', 'lakshan@gmail.com', 772345678, 'lakshan123', 'Seller', 'What is your Birthdate?', 'sep 09 2001'),
(4, 'shavin', 'meepagala', 'shavin@gmail.com', 778912314, 'shavin123', 'Seller', 'What is your Birthdate?', 'aug 31st'),
(5, 'shawn', 'Perera', 'shawn@gmail.com', 779861243, 'shawn123', 'admin', 'What is your Pet Name?', 'Ben'),
(6, 'Ashinka', 'Meepagala', 'shavinmeepagala@gmail.com', 779865423, 'meepagala123', 'Buyer', 'What is your Birthdate?', 'aug 31 2001'),
(9, 'Nimesh ', 'dasan', 'Nimesh@gmail.cm', 702767914, 'nimesh123', 'Buyer', 'What is your Birthdate?', 'may 5 1995'),
(10, 'Imesha', 'desilva', 'imesha@gmail.com', 773452167, 'imesha123', 'Seller', 'What is your Birthdate?', 'oct 16 2000');

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

DROP TABLE IF EXISTS `system_settings`;
CREATE TABLE IF NOT EXISTS `system_settings` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `cover_img` text NOT NULL,
  `about_content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`id`, `name`, `email`, `contact`, `cover_img`, `about_content`) VALUES
(1, 'Simple Online Bidding System', 'info@sample.comm', '+6948 8542 623', '1603344720_1602738120_pngtree-purple-hd-business-banner-image_5493.jpg', '&lt;p style=&quot;text-align: center; background: transparent; position: relative;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-weight: 400; text-align: justify;&quot;&gt;&amp;nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&rsquo;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;p style=&quot;text-align: center; background: transparent; position: relative;&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;p style=&quot;text-align: center; background: transparent; position: relative;&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE IF NOT EXISTS `vehicle` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Brand_Name` varchar(100) NOT NULL,
  `Model` varchar(100) NOT NULL,
  `Category` varchar(100) NOT NULL,
  `Price` int(100) NOT NULL,
  `Milage` varchar(100) NOT NULL,
  `User` varchar(100) NOT NULL,
  `Transmission` varchar(100) NOT NULL,
  `FuelType` varchar(50) NOT NULL,
  `Vehicle_Image` varchar(1000) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`Id`, `Brand_Name`, `Model`, `Category`, `Price`, `Milage`, `User`, `Transmission`, `FuelType`, `Vehicle_Image`) VALUES
(7, 'Ferrari', '2.0', 'Car', 20000000, '5000', '2nd owner', 'Auto Transmission', 'Petrol', '2013 f (1).jpg'),
(8, 'Vega', '1.0', 'Car', 10000000, '10,000', '1st Owner', 'Auto Transmission', 'Electric', '0x0.jpg'),
(9, 'Kawasaki', '3.0', 'Bike', 5000000, '1000', '1st owner', 'Auto Transmission', 'Petrol', 'R (1).jpeg'),
(10, 'Kawasaki', 'ZX6R', 'Bike', 5000000, '2000', '1st Owner', 'Auto Transmission', 'Petrol', 'R.jpeg'),
(11, 'Sea', '300', 'JetSki', 10000000, '5000', '1st Owner', 'Auto Transmission', 'Petrol', 'R (3).jpeg'),
(12, 'Sea', '394', 'JetSki', 10000000, '2000', '1st owner', 'Auto Transmission', 'Petrol', 'R (2).jpeg'),
(13, 'Benz', '2.0', 'Classic', 2000000, '100,000.00', '1st Owner', 'Manual Transmission', 'Diesel', 'R (5).jpeg'),
(16, 'Mustang', '2.0', 'Classic', 5000000, '100,000', '2nd owner', 'Manual Transmission', 'Diesel', 'R (4).jpeg'),
(17, 'Bently', '3.0', 'Car', 4000000, '100000', '2nd owner', 'Manual Transmission', 'Diesel', 'merlin_214812261_9deba3ce-a3d3-4b44-b652-e16406fc4988-videoSixteenByNine3000.jpg'),
(19, 'Aston Martin', 'V8 Vantage base', 'Car', 18000000, '10 374', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot (1981).png'),
(20, 'Ferrari', '812 GTS', 'Car', 252000000, '1000', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot (1982).png'),
(21, 'Kawasaki', 'Ultra 310LX', 'Jet Ski', 10000000, '1100', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot (1980).png'),
(22, 'Sea Doo', 'Spark Trixx', 'Jet Ski', 12000000, '1200', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot (1984).png'),
(23, 'Maserati', 'Ghibli GT', 'Car', 21600000, '12300', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot (1989).png'),
(24, 'McLaren', '570s Base', 'Car', 55800000, '28000', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot 1(1990).png'),
(25, 'Sea Doo', 'Spark 60 HP', 'Jet Ski', 14000000, '10,000', '1st Owner', 'Auto Transmission', 'Petrol', 'sea-doo-apark-60-hp-featured-image-1140x760.jpg'),
(26, 'Yamaha', 'FX Cruiser Ho', 'Jet Ski', 16000000, '12000', '1st Owner', 'Auto Transmission', 'Petrol', 'yamaha-fx-cruiser-ho-featured-image-1140x760.jpg'),
(27, 'Lamborghini', 'Urus Base', 'Car', 72000000, '27000', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot (1988).png'),
(28, 'Porsche', 'Macan Base', 'Car', 23400000, '18000', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot (1983).png'),
(29, 'Honda', 'CBR', 'Jet Ski', 1360000, '50000', '1st Owner', 'Auto Transmission', 'Petrol', 'fitted.jpg'),
(30, 'Honda', 'CBR650R', 'Bike', 1000000, '1000', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot (1979).png'),
(31, 'Sea Doo', 'GTI SE', 'Jet Ski', 10000000, '11000', '1st Owner', 'Auto Transmission', 'Petrol', 'Screenshot 1(1986).png');

-- --------------------------------------------------------

--
-- Table structure for table `vehiclebid`
--

DROP TABLE IF EXISTS `vehiclebid`;
CREATE TABLE IF NOT EXISTS `vehiclebid` (
  `bidid` int(30) NOT NULL AUTO_INCREMENT,
  `UserId` int(30) NOT NULL,
  `brandname` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `milage` varchar(100) NOT NULL,
  `manyear` date DEFAULT NULL,
  `transmission` varchar(10) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `CountryofMan` varchar(20) NOT NULL,
  `regprice` varchar(20) NOT NULL,
  `bidstartamt` varchar(20) NOT NULL,
  `bidenddate` date NOT NULL,
  `Bid_image` varchar(100) NOT NULL,
  PRIMARY KEY (`bidid`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehiclebid`
--

INSERT INTO `vehiclebid` (`bidid`, `UserId`, `brandname`, `model`, `milage`, `manyear`, `transmission`, `owner`, `CountryofMan`, `regprice`, `bidstartamt`, `bidenddate`, `Bid_image`) VALUES
(11, 0, 'mustang', '2x80', '100000', '1995-06-07', 'Manual', '2nd owner', 'America', '2000000', '1800000', '2022-12-30', 'R (4).jpeg'),
(15, 4, 'BMW', '507 Series', '100000', '1956-06-05', 'Manual', '2nd owner', 'America', '2000000', '2000000', '2023-01-08', 'bmw.jpeg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
