
<?php
include('authentication.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Boxicons -->
	<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
	<!-- My CSS -->
	<link rel="stylesheet" href="admin.css">

	<title>AdminHub</title>
</head>
<body>


	<!-- SIDEBAR -->
	<section id="sidebar">
		<a href="admin (2).php" class="brand">
			<i class='bx bxs-user'></i>
			<span class="text">AdminHub</span>
		</a>
		<ul class="side-menu top">
			<li>
				<a href="admin (2).php">
					<i class='bx bxs-dashboard' ></i>
					<span class="text">Dashboard</span>
				</a>
			</li>
			<li>
				<a href="Aproduct.php">
					<i class='bx bxl-product-hunt' ></i>
					<span class="text">Products</span>
				</a>
			</li>
			<li class="active">
				<a href="Adminbidding.php">
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">Manage Bidding</span>
				</a>
			</li>
			<li>
				<a href="Amembers.php">
					<i class='bx bxs-user-badge' ></i>
					<span class="text">Manage Members</span>
				</a>
			</li>
			<li>
				<a href="Ateam.php">
					<i class='bx bxs-group' ></i>
					<span class="text">Team</span>
				</a>
			</li>
		</ul>
		<ul class="side-menu">
			
			<li>
				<form action="Alogout.php" method="POST">
					<i class='bx bxs-log-out-circle' ></i>
              <button type="submit" name="logout" class="logout">Logout</button>
            </form>
			</li>
		</ul>
	</section>
	<!-- SIDEBAR -->
    <section id="content">
		<!-- NAVBAR -->
		<nav>
			<i class='bx bx-menu' ></i>
			<a href="#" class="nav-link">Categories</a>
			<form method="post" action="searchimgnew.php" id="searchform">
				<div class="form-input">
					<input type="text" name="Brand_Name" id="Brand_Name" placeholder="Search...">
					<button input type="submit" name="submit" class="search-btn"><i class='bx bx-search' ></i></button>
				</div>
			</form>
        </nav>
    </section>    
	<!-- CONTENT -->
	<section id="content">
		
		<!-- MAIN -->
		<main>
			<div class="head-title">
				<div class="left">
					<h1>Bidding Management</h1>
					
				</div>
				
			</div>

			<ul class="box-info">
				<li>
					<i class='bx bxs-calendar-check' ></i>
					<span class="text">
						<h3><a href="bids.php">	<input type="submit" name="Bidding Management" value="Bidding Management"></a></h3>
					</span>
				</li>
				<li>
					<i class='bx bxs-group' ></i>
					<span class="text">
						<h3><a href="products.php">	<input type="submit" name="Bidding Items" value="Bidding Items"></a></h3>
						
					</span>
				</li>
				<li>
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">
						<h3><a href="adminbidtable.php">	<input type="submit" name="Bidding Items" value="Bidding Items"></a>	</h3>
						
					</span>
				</li>
			</ul>
		</main>

	<script src="script.js"></script>
</body>
</html>