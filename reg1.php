<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="reg.css">
  <link rel="stylesheet" href="foot.css">
  
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="#"><img src="media/auto_wheels-removebg-preview.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <img src="media/menu.png" alt="">
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto text-right">
              <li class="nav-item ">
                <a class="nav-link" href="indexx.php">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="filter1 (2).php">sale</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="viewbid.php">Bidding</a>
              </li>
                <li class="nav-item">

 <div class="w3-container">
				 <div class="w3-dropdown-click">
   <?php
				if(isset($_SESSION['auth'])){
					echo " </li>
				<li class='nav-item'>
				<form action='Alogout.php' method='POST'>
					<i class='bx bxs-log-out-circle' ></i>
              <button type='submit' name='logout' class='logout'>Sign out</button>
            </form>
              </li>";
				}
				else{
					echo " </li>
				<li class='nav-item'>
				<form action='login.php' method='POST'>
					<i class='bx bxs-log-out-circle' ></i>
              <button type='submit' name='logout' class='logout'>Sign Up</button>
            </form>
              </li>";
				}
				?>
  </div>
				  </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <section id="reg">
        <div class="container register">
          <div class="row">
              <div class="col-md-3 register-left">
                  <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                  <h3>Welcome</h3>
                  <p>You are 30 seconds away from earning your own money!</p>
                <a href="login.php">  <input type="submit" name="" value="Login"/><br/></a>
              </div>
              <div class="col-md-9 register-right">
               
                  <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                          <h3 class="register-heading">Apply as a Seller</h3>  
						  <form name="regsell_form" method="post" action="regsell.php">
                          <div class="row register-form" >
							
                              <div class="col-md-6">
                                  <div class="form-group" >
                                      <input type="text" class="form-control" name="fname" placeholder="First Name *" value="" />
                                  </div>
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="lname" placeholder="Last Name *" value="" />
                                  </div>
                                  <div class="form-group">
                                      <input type="password" class="form-control" name="password" placeholder="Password *" value="" />
                                  </div>
                                  <div class="form-group">
                                      <input type="password" class="form-control" name="conpassword" placeholder="Confirm Password *" value="" />
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <input type="email" class="form-control" name="email" placeholder="Your Email *" value="" />
                                  </div>
                                  <div class="form-group">
                                      <input type="text" minlength="10" maxlength="10" name="phone" class="form-control" placeholder="Your Phone *" value="" />
                                  </div>
								   <div class="form-group">
                                      <select name="UserType" id="UserType" class="form-control">  
                                          <option value="Buyer">Buyer</option>
                                          <option  value="Seller">Seller</option>
                                      </select>
                                  </div>
                                  <div class="form-group">
                                      <select name="secquestion" id="secquestion" class="form-control">
                                          <option value="What is your Birthdate?">What is your Birthdate?</option>
                                          <option value="What is Your old Phone Number">What is Your old Phone Number</option>
                                          <option value="What is your Pet Name?">What is your Pet Name?</option>
                                      </select>
                                  </div>
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="answer" placeholder="Enter Your Answer *" value="" />
                                  </div>
								  <input type="submit" onClick="submit_box()" class="btnRegister" name="submit" value="Submit"/>
                                 
                              </div>
							  
                          </div></form>
                      </div>
         
                  </div>
              </div>
          </div>
        
        </div>
        </section>
</body>
</html>