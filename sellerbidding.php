<?php
include('authenticationseller.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Boxicons -->
	<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
	<!-- My CSS -->
	<link rel="stylesheet" href="admin.css">
	<link rel="stylesheet" href="bidding.css">

	<title>SellerHub</title>
</head>
<body>


	<!-- SIDEBAR -->
	<section id="sidebar">
		<a href="#" class="brand">
			<i class='bx bxs-user'></i>
			<span class="text">SellerHub</span>
		</a>
		<ul class="side-menu top">
			<li>
				<a href="sellerpanel.php">
					<i class='bx bxs-dashboard' ></i>
					<span class="text">Dashboard</span>
				</a>
			</li>
			<li>
				<a href="Sproduct.php">
					<i class='bx bxl-product-hunt' ></i>
					<span class="text">Manage Products</span>
				</a>
			</li>
			<li  class="active">
				<a href="sellerbidding.php">
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">Manage Bidding</span>
				</a>
			</li>
		</ul>
		<ul class="side-menu">
			
			<li>
				<i class='bx bxs-log-out-circle' >
				<form action="Alogout.php" method="POST">
              <button type="submit" name="logout" class="dropdown-item">Logout</button>
</i>
            </form>	
			</li>
		</ul>
	</section>
	<!-- SIDEBAR -->



	<!-- CONTENT -->
	<section id="content">
		<!-- NAVBAR -->
		<nav>
			<i class='bx bx-menu' ></i>
			<a href="#" class="nav-link">Categories</a>
			<form method="post" action="searchimgnew.php" id="searchform">
				<div class="form-input">
					<input type="text" name="Brand_Name" id="Brand_Name" placeholder="Search...">
					<button input type="submit" name="submit" class="search-btn"><i class='bx bx-search' ></i></button>
				</div>
			</form>
			
		</nav>
		<!-- NAVBAR -->

		<!-- MAIN -->
		<main>
			<div class="head-title">
				<div class="left">
					<h1>Add Bidding</h1>
					
				</div>
				
			</div>

			<!--<ul class="box-info">
				<li>
					<i class='bx bxs-calendar-check' ></i>
					<span class="text">
						<h3>1020</h3>
						<p>New Order</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-group' ></i>
					<span class="text">
						<h3>2834</h3>
						<p>Visitors</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">
						<h3>$2543</h3>
						<p>Total Sales</p>
					</span>
				</li>
			</ul>-->


			<!--<div class="table-data">
				<div class="order">
					<div class="head">
						<h3>Recent Orders</h3>
						<i class='bx bx-search' ></i>
						<i class='bx bx-filter' ></i>
					</div>
					<table>
						<thead>
							<tr>
								<th>User</th>
								<th>Date Order</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<img src="img/people.png">
									<p>John Doe</p>
								</td>
								<td>01-10-2021</td>
								<td><span class="status completed">Completed</span></td>
							</tr>
							<tr>
								<td>
									<img src="img/people.png">
									<p>John Doe</p>
								</td>
								<td>01-10-2021</td>
								<td><span class="status pending">Pending</span></td>
							</tr>
							<tr>
								<td>
									<img src="img/people.png">
									<p>John Doe</p>
								</td>
								<td>01-10-2021</td>
								<td><span class="status process">Process</span></td>
							</tr>
							<tr>
								<td>
									<img src="img/people.png">
									<p>John Doe</p>
								</td>
								<td>01-10-2021</td>
								<td><span class="status pending">Pending</span></td>
							</tr>
							<tr>
								<td>
									<img src="img/people.png">
									<p>John Doe</p>
								</td>
								<td>01-10-2021</td>
								<td><span class="status completed">Completed</span></td>
							</tr>
						</tbody>
					</table>
				</div>-->		
		</main>
		<!-- MAIN -->
		<div class="container">	
		<form action="bid.php" method="post" enctype="multipart/form-data" class="form1">
        <div class="input-box">
          <label>Brand Name</label>
          <input type="text" name="brandname" placeholder="Brand Name" required />
        </div>
        <div class="input-box">
          <label>Model</label>
          <input type="text" name="model" placeholder="Enter Model" required />
        </div>
        <div class="column">
          <div class="input-box">
            <label>Milage</label>
            <input type="Text" name="milage" placeholder="Enter Milage" required />
          </div>	 
        </div>
        <div class="transmission-box">
          <h3>Transmission</h3>
			<div class="column">
            <div class="select-box">
              <select name="transmission">
                <option hidden>Transmission</option>
                <option value="Auto">Auto Transmission</option>
                <option value="Manual">Manual Transmission</option>
              </select>
            </div>         
          </div>
        </div>
			<div formclass="input-box">
            <label>Manufacture year</label>
            <input type="date" name="manyear" placeholder="Manufacture year" required />
          </div>
        <div class="input-box address">
          <label>Owner</label>
          <input type="text" name="owner" placeholder="Enter Owner" required />
          
          <div class="column">
            <div class="select-box">
              <select name="CountryofMan">
                <option hidden>Country of Manufacture</option>
                <option value="America">America</option>
                <option value="Japan">Japan</option>
                <option value="UK">UK</option>
                <option value="German">German</option>
				  <option value="India">India</option>
				  <option value="China">China</option>
              </select>
            </div>
           
          </div>
          <div class="column">
            <input type="number" name="regprice" placeholder="Regular Price" required />
            <input type="number" name="bidstartamt" placeholder="Bidding Start Amount" required />
          </div>
        </div>
			 <div class="input-box">
            <label>Bidding End Date</label>
            <input type="date" name="bidenddate" placeholder="Bidding start date" required />
			</div>
			<div>
       <label>Bidding Vehicle Image</label>
        <label for="img"></label>
        <label for="Bid_image"></label>
        <label for="img2"></label>
        <input type="hidden" name="size" value="1000000">
        <input type="file" name="Bid_image" id="img2" /><!---allowuser choose one or more files from their device storage. Once chosen, the files can be uploaded to a server -->
			</div>
        <button input type="submit" name="submit" id="submit" value="Submit" />submit</button>
      </form>
		</div>
	</section>
	<!-- CONTENT -->


	<script src="script.js"></script>
</body>
</html>