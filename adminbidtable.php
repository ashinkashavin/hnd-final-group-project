<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet" href="product.css">
</head>
<body>
<?php
include('authentication.php');
?>
<div>
	
	<div class="col-lg-12">
		<div class="row mb-4 mt-4">
			<div class="col-md-12">
				
			</div>
		</div>
		<div class="row">
			<!-- FORM Panel -->

			<!-- Table Panel -->
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h1>List of Products</h1>
					</div>
					<div class="card-body">
					<?php
	echo '<p><a href="javascript:history.go (-1)" title="Return to previous page">« Go back</a></p>';?>
						<table class="table table-condensed table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="">Img</th>
									<th class="">Product</th>
									<th class="">Other Info</th>
									
								</tr>
							</thead>
							<tbody>
								<?php 
								$i = 1;
								
								$products = $conn->query("SELECT * FROM vehiclebid order by brandname asc ");
								while($row=$products->fetch_assoc()):
									$get = $conn->query("SELECT * FROM bids where bidid = {$row['bidid']} order by bid_amount desc limit 1 ");
									$bid = $get->num_rows > 0 ? $get->fetch_array()['bid_amount'] : 0 ;
									$tbid = $conn->query("SELECT distinct(UserId) FROM bids where bidid = {$row['bidid']} ")->num_rows;
								?>
								<tr data-id= '<?php echo $row['bidid'] ?>'>
									<td class="text-center"><?php echo $i++ ?></td>
									<td class="">
										 <div class="row justify-content-center">
										 	<img src="<?php echo 'upload/'.$row['Bid_image'] ?>" alt="">
										 </div>
									</td>
									<td class="">
										 <p>Name: <b><?php echo ucwords($row['brandname']) ?></b></p>
										
									</td>
									<td>
										 <p><small>Regular Price: <b><?php echo number_format($row['regprice'],2) ?></b></small></p>
										 <p><small>Start Price: <b><?php echo number_format($row['bidstartamt'],2) ?></b></small></p>
										 <p><small>End Date/Time: <b><?php echo date("M d,Y h:i A",strtotime($row['bidenddate'])) ?></b></small></p>
										 <p><small>Highest Bid: <b class="highest_bid"><?php echo number_format($bid,2) ?></b></small></p>
										 <p><small>Total Bids: <b class="total_bid"><?php echo $tbid ?> user/s</b></small></p>
									</td>
								</tr>
								<?php endwhile; ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
			<!-- Table Panel -->
		</div>
	</div>	

</div>
<style>
	
	td{
		vertical-align: middle !important;
	}
	td p{
		margin: unset
	}
	table td img{
		max-width:100px;
		max-height: :150px;
	}
	img{
		max-width:100px;
		max-height: :150px;
	}
</style>
<script>
	$(document).ready(function(){
		$('table').dataTable()
	})
	
	$('.view_product').click(function(){
		uni_modal("product Details","view_product.php?id="+$(this).attr('data-id'),'mid-large')
		
	})
	$('.edit_product').click(function(){
		location.href ="index.php?page=manage_product&id="+$(this).attr('data-id')
		
	})
	$('.delete_product').click(function(){
		_conf("Are you sure to delete this product?","delete_product",[$(this).attr('data-id')])
	})
	
	function delete_product($id){
		start_load()
		$.ajax({
			url:'ajax.php?action=delete_product',
			method:'POST',
			data:{id:$id},
			success:function(resp){
				if(resp==1){
					alert_toast("Data successfully deleted",'success')
					setTimeout(function(){
						location.reload()
					},1500)

				}
			}
		})
	}
</script>
</body>
</html>
