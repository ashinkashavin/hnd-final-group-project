<?php
include('authenticationseller.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Boxicons -->
	<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
	<!-- My CSS -->
	<link rel="stylesheet" href="admin.css">

	<title>SellerHub</title>
</head>
<body>
<?php
if(isset($_SESSION['UserType'])){
	
	echo $_SESSION['UserType'];
}
	else{
		echo"Session not created";
	}
?>

	<!-- SIDEBAR -->
	<section id="sidebar">
		<a href="#" class="brand">
			<i class='bx bxs-user'></i>
			<span class="text">SellerHub</span>
		</a>
		<ul class="side-menu top">
			<li class="active">
				<a href="sellerpanel.php">
					<i class='bx bxs-dashboard' ></i>
					<span class="text">Dashboard</span>
				</a>
			</li>
			<li>
				<a href="Sproduct.php">
					<i class='bx bxl-product-hunt' ></i>
					<span class="text">Manage Products</span>
				</a>
			</li>
			<li>
				<a href="sellerbidding.php">
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">Manage Bidding</span>
				</a>
			</li>
		</ul>
		<ul class="side-menu">
			
			<li>
				<i class='bx bxs-log-out-circle' >
				<form action="Alogout.php" method="POST">
              <button type="submit" name="logout" class="dropdown-item">Logout</button>
</i>
            </form>	
			</li>
		</ul>
	</section>
	<!-- SIDEBAR -->



	<!-- CONTENT -->
	<section id="content">
		<!-- NAVBAR -->
		<nav>
			<i class='bx bx-menu' ></i>
			<a href="#" class="nav-link">Categories</a>
			<form method="post" action="searchimgnew.php" id="searchform">
				<div class="form-input">
					<input type="text" name="Brand_Name" id="Brand_Name" placeholder="Search...">
					<button input type="submit" name="submit" class="search-btn"><i class='bx bx-search' ></i></button>
				</div>
			</form>
			
		</nav>
		<!-- NAVBAR -->

		<!-- MAIN -->
		<main>
			<div class="head-title">
				<div class="left">
					<h1>Dashboard</h1>
					
				</div>
				
			</div>

			<!--<ul class="box-info">
				<li>
					<i class='bx bxs-calendar-check' ></i>
					<span class="text">
						<h3>1020</h3>
						<p>New Order</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-group' ></i>
					<span class="text">
						<h3>2834</h3>
						<p>Visitors</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">
						<h3>$2543</h3>
						<p>Total Sales</p>
					</span>
				</li>
			</ul>-->


			<div class="table-data">
				<div class="order">
					<div class="head">
						<h3>Recent Orders</h3>
						<i class='bx bx-search' ></i>
						<i class='bx bx-filter' ></i>
					</div>
					<table>
						<thead>
							<tr>
								<th class="text-center">#</th>
									<th class="">Img</th>
									<th class="">Product</th>
									<th class="">Other Info</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$i = 1;
								
								$products = $conn->query("SELECT * FROM vehiclebid Where UserId={$_SESSION['auth_user']['user_id']} order by brandname asc ");
								while($row=$products->fetch_assoc()):
									$get = $conn->query("SELECT * FROM bids where bidid = {$row['bidid']} order by bid_amount desc limit 1 ");
									$bid = $get->num_rows > 0 ? $get->fetch_array()['bid_amount'] : 0 ;
									$tbid = $conn->query("SELECT distinct(UserId) FROM bids where bidid = {$row['bidid']} ")->num_rows;
								?>
								<tr data-id= '<?php echo $row['bidid'] ?>'>
								<td class="text-center"><?php echo $i++ ?></td>
								<td>01-10-2021</td>
								<td class="">
										 <div class="row justify-content-center">
										 	<img src="<?php echo 'upload/'.$row['Bid_image'] ?>" alt="">
										 </div>
									</td>
								<td class="">
										 <p>Name: <b><?php echo ucwords($row['brandname']) ?></b></p>
										
									</td>
								<td>
										 <p><small>Regular Price: <b><?php echo number_format($row['regprice'],2) ?></b></small></p>
										 <p><small>Start Price: <b><?php echo number_format($row['bidstartamt'],2) ?></b></small></p>
										 <p><small>End Date/Time: <b><?php echo date("M d,Y h:i A",strtotime($row['bidenddate'])) ?></b></small></p>
										 <p><small>Highest Bid: <b class="highest_bid"><?php echo number_format($bid,2) ?></b></small></p>
										 <p><small>Total Bids: <b class="total_bid"><?php echo $tbid ?> user/s</b></small></p>
									</td>
							</tr>
					<?php endwhile; ?>
						</tbody>
					</table>
				</div>
				
		</main>
		<!-- MAIN -->
	</section>
	<!-- CONTENT -->
	

	<script src="script.js"></script>
</body>
</html>