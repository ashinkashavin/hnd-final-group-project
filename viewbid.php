<?php
session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- CSS only -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="foot.css">
	<link rel="stylesheet" href="pp1.css"/>

</head>

<body>
	  <div class="container1">
        <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="#"><img src="media/auto_wheels-removebg-preview.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <img src="media/menu.png" alt="">
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto text-right">
              <li class="nav-item ">
                <a class="nav-link" href="indexx.php">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="filter1 (2).php">Sale</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="viewbid.php">Biding</a>
              </li>
              <li class="nav-item">

 <div class="w3-container">
				 <div class="w3-dropdown-click">
    <?php
				if(isset($_SESSION['auth'])){
					echo " </li>
				<li class='nav-item'>
				<form action='Alogout.php' method='POST'>
					<i class='bx bxs-log-out-circle' ></i>
              <button type='submit' name='logout' class='logout'>Sign out</button>
            </form>
              </li>";
				}
				else{
					echo " </li>
				<li class='nav-item'>
				<form action='login.php' method='POST'>
					<i class='bx bxs-log-out-circle' ></i>
              <button type='submit' name='logout' class='logout'>Sign Up</button>
            </form>
              </li>";
				}
				?>
  </div>
				  </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
	  <?php
	include 'Connection.php';
	
	$result = mysqli_query($conn,"SELECT vb.*, MAX(b.bid_amount) AS highest_bid_amount
FROM vehiclebid vb
  JOIN bids b ON b.bidid=vb.bidid
GROUP BY vb.bidid");
	?>
	
			<?php
			while($row = mysqli_fetch_array($result)) {
			?>
	 
		 <div class="lrow">
		<div class="vehicle">
    <form name="Viewbid_form" method='post' action='bidinfo.php'enctype="multipart/form-data" >
		<div class='image'> <?php
		 echo "<img src='".$row['Bid_image']."' />"?></div>
		 <input type="hidden"  class='Pname'name="bidid" value=<?php echo $row["bidid"]; ?>></input> 
		 <div class='des'name="brandname"> Brand Name:<p><?php echo $row["brandname"]; ?></p></div>
		 <div class='des'name="model"> Model:<p><?php echo $row["model"]; ?> </p></div>
		<div class='des'name="manyear"> Manufacture Year:<p><?php echo $row["manyear"]; ?></p></div>
		 <div class='des'name="bidstartamt">Bid Start Amount: <p><?php echo  $row["bidstartamt"]; ?></p></div>
		 <div class='des'name="bidenddate">Bid End Date: <p><?php echo date($row["bidenddate"] )?></p></div>
		<div class='des'name="model"> Highest Bid:<p><?php echo (number_format((float)"bidstartamt",2)) ?> </p></div>
				<div class="form-group">
					<label for="" class="control-label">Bid Amount</label>
					<input type="number" class="form-control text-right" name="bid_amount" >
				</div>
				<div class="btn">
					 <button
    class="bton"
    name="submit"
    id="submit"
    <?php 
        echo time() > DateTime::createFromFormat('Y-m-d H:i:s', $row['bidenddate'] . ' 23:59:59')->getTimestamp()
            ? 'disabled'
            : ''   
    ?>
>
    Submit
</button>
					 <button input type="reset" class="bton" name="button" id="cancel_bid">Cancel</button>
				</div>
			
		 
    </form>
</div>
	</div>
			<?php
			}
			?>
	
</body>
</html>