<?php
include('authentication.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Boxicons -->
	<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
	<!-- My CSS -->
	<link rel="stylesheet" href="admin.css">

	<title>AdminHub</title>
</head>
<body>


	<!-- SIDEBAR -->
	<section id="sidebar">
		<a href="admin (2).php" class="brand">
			<i class='bx bxs-user'></i>
			<span class="text">AdminHub</span>
		</a>
		<ul class="side-menu top">
			<li>
				<a href="admin (2).php">
					<i class='bx bxs-dashboard' ></i>
					<span class="text">Dashboard</span>
				</a>
			</li>
			<li>
				<a href="Aproduct.php">
					<i class='bx bxl-product-hunt' ></i>
					<span class="text">Products</span>
				</a>
			</li>
			<li>
				<a href="Adminbidding.php">
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">Manage Bidding</span>
				</a>
			</li>
			<li class="active">
				<a href="Amembers.php">
					<i class='bx bxs-user-badge' ></i>
					<span class="text">Manage Members</span>
				</a>
			</li>
			<li>
				<a href="Ateam.php">
					<i class='bx bxs-group' ></i>
					<span class="text">Team</span>
				</a>
			</li>
		</ul>
		<ul class="side-menu">
			
			<li>
				<form action="Alogout.php" method="POST">
					<i class='bx bxs-log-out-circle' ></i>
              <button type="submit" name="logout" class="logout">Logout</button>
            </form>
			</li>
		</ul>
	</section>
	<!-- SIDEBAR -->
    <section id="content">
		<!-- NAVBAR -->
		<nav>
			<i class='bx bx-menu' ></i>
			<a href="#" class="nav-link">Categories</a>
			<form method="post" action="searchimgnew.php" id="searchform">
				<div class="form-input">
					<input type="text" name="Brand_Name" id="Brand_Name" placeholder="Search...">
					<button input type="submit" name="submit" class="search-btn"><i class='bx bx-search' ></i></button>
				</div>
			</form>
        </nav>
    </section>    
	<!-- CONTENT -->
	<section id="content">
		
		<!-- MAIN -->
		<main>
			<div class="head-title">
				<div class="left">
					<h1>Seller Management</h1>
					
				</div>
				
			</div>

			<ul class="box-info">
				<li>
					<i class='bx bxs-calendar-check' ></i>
					<span class="text">
						<h3>1020</h3>
						<p>New Order</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-group' ></i>
					<span class="text">
						<h3>2834</h3>
						<p>Visitors</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">
						<h3>$2543</h3>
						<p>Total Sales</p>
					</span>
				</li>
			</ul>
			<div class="table-data">
				<div class="order">
					<div class="head">
						<h3>Recent Orders</h3>
						<i class='bx bx-search' ></i>
						<i class='bx bx-filter' ></i>
					</div>
					<table>
						<thead>
							<tr>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Phone</th>
								<th>User Type</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php
	include 'Connection.php';
	
	$result = mysqli_query($conn,"SELECT * FROM regbuyer " );
	?>
							<?php
			while($row = mysqli_fetch_array($result)) {
			?>
							<tr>
								
									<form method='post' action=''>
		
	 <td><div class='Pname'>  <p>    <?php echo $row["fname"]; ?></p></div></td>
		<td> <div class='des'> <p>	<?php echo $row["lname"]; ?></p></div></td>
		<td><div class='des'> <p>	<?php echo $row["email"]; ?></p></div></td>
										<td> <div class='des'> <p>	<?php echo $row["phone"]; ?></p></div></td>
		<td><div class='des'> <p>	<?php echo $row["UserType"]; ?></p></div></td>
										 <td><a href="Deleteuser.php?del=<?php echo $row['UserId'];?>">Delete</a></td>	
    </form>
							</tr>
							<?php
			}
			?>
							
						</tbody>
					</table>
				</div> 
		</main>

	<script src="script.js"></script>
</body>
</html>