<?php
include('authentication.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Boxicons -->
	<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
	<!-- My CSS -->
	<link rel="stylesheet" href="admin.css">
	<link rel="stylesheet" href="Ateamreg.css">

	<title>AdminHub</title>
</head>	
<body>


	<!-- SIDEBAR -->
	<section id="sidebar">
		<a href="admin (2).php" class="brand">
			<i class='bx bxs-user'></i>
			<span class="text">AdminHub</span>
		</a>
		<ul class="side-menu top">
			<li>
				<a href="admin (2).php">
					<i class='bx bxs-dashboard' ></i>
					<span class="text">Dashboard</span>
				</a>
			</li>
			<li>
				<a href="Aproduct.php">
					<i class='bx bxl-product-hunt' ></i>
					<span class="text">Products</span>
				</a>
			</li>
			<li>
				<a href="Adminbidding.php">
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">Manage Bidding</span>
				</a>
			</li>
			<li>
				<a href="Amembers.php">
					<i class='bx bxs-user-badge' ></i>
					<span class="text">Manage Members</span>
				</a>
			</li>
			<li class="active">
				<a href="Ateam.php">
					<i class='bx bxs-group' ></i>
					<span class="text">Team</span>
				</a>
			</li>
		</ul>
		<ul class="side-menu">
		
			<li>
				<form action="Alogout.php" method="POST">
					<i class='bx bxs-log-out-circle' ></i>
              <button type="submit" name="logout" class="logout">Logout</button>
            </form>
			</li>
		</ul>
	</section>
	<!-- SIDEBAR -->
    <section id="content">
		<!-- NAVBAR -->
		<nav>
			<i class='bx bx-menu' ></i>
			<a href="#" class="nav-link">Categories</a>
			<form method="post" action="searchimgnew.php" id="searchform">
				<div class="form-input">
					<input type="text" name="Brand_Name" id="Brand_Name" placeholder="Search...">
					<button input type="submit" name="submit" class="search-btn"><i class='bx bx-search' ></i></button>
				</div>
			</form>
        </nav>
    </section>    
	<!-- CONTENT -->
	<section id="content">
		
		<!-- MAIN -->
		<main>
			<div class="head-title">
				<div class="left">
					<h1>Team Management</h1>
					
				</div>
				
			</div>

			<ul class="box-info">
				<li>
					<i class='bx bxs-calendar-check' ></i>
					<span class="text">
						<h3>1020</h3>
						<p>New Order</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-group' ></i>
					<span class="text">
						<h3>2834</h3>
						<p>Visitors</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">
						<h3>$2543</h3>
						<p>Total Sales</p>
					</span>
				</li>
			</ul>
			<div class="testbox">
  <h1>Registration</h1>

   <form name="regsell_form" method="post" action="reg.php">
                          <div class="row register-form" >
							
                              <div class="col-md-6">
                                  <div class="form-group" >
                                      <input type="text" class="form-control" name="fname" placeholder="First Name *" value="" />
                                  </div>
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="lname" placeholder="Last Name *" value="" />
                                  </div>
                                  <div class="form-group">
                                      <input type="password" class="form-control" name="password" placeholder="Password *" value="" />
                                  </div>
                                  <div class="form-group">
                                      <input type="password" class="form-control" name="conpassword" placeholder="Confirm Password *" value="" />
                                  </div>
								  <div class="form-group">
                                      <input type="email" class="form-control" name="email" placeholder="Your Email *" value="" />
                                  </div>
								  <div class="form-group">
                                      <input type="text" minlength="10" maxlength="10" name="phone" class="form-control" placeholder="Your Phone *" value="" />
                                  </div>
                                  <div class="form-group">
                                      <select name="UserType" class="form-control">
                                         
                                          <option value="admin">Admin</option>
                                          <option  value="Seller">Seller</option>
                                      </select>
                                  </div>
								  <div class="form-group">
                                      <select name="secquestion" class="form-control">
                                         
                                          <option value="What is your Birthdate?">What is your Birthdate?</option>
                                          <option value="What is Your old Phone Number">What is Your old Phone Number</option>
                                          <option value="What is your Pet Name?">What is your Pet Name?</option>
                                      </select>
                                  </div>
                              </div>
							  <div class="form-group">
                                <input type="text" class="form-control" name="answer" placeholder="Enter Your Answer *" value="" /><br>
								<input type="submit" onClick="submit_box()" class="btnRegister btn-4" name="submit" value="Submit"/>
								<span></span>
								</div>
                              </div>
    </form>
</div>
		</main>

	<script src="script.js"></script>
</body>
</html>