<?php 
session_start();
include('Connection.php');

if(!isset($_SESSION['auth'])){

    $_SESSION['message'] = "login to Access Dashboard";
    header("Location: login.php ");
    exit(0);

}else{

    if($_SESSION['auth_role'] != "Buyer"){

        $_SESSION['message'] = "You are not Authorized as Buyer";
        header("Location: login.php ");
        exit(0);
    
    }

}




?>