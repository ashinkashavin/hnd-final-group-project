<?php
include('authentication.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Boxicons -->
	<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
	<!-- My CSS -->
	<link rel="stylesheet" href="Aproduct.css">

	<title>AdminHub</title>
</head>
<body>


	<!-- SIDEBAR -->
	<section id="sidebar">
		<a href="admin (2).php" class="brand">
			<i class='bx bxs-user'></i>
			<span class="text">AdminHub</span>
		</a>
		<ul class="side-menu top">
			<li>
				<a href="admin (2).php">
					<i class='bx bxs-dashboard' ></i>
					<span class="text">Dashboard</span>
				</a>
			</li>
			<li  class="active">
				<a href="Aproduct.php">
					<i class='bx bxl-product-hunt' ></i>
					<span class="text">Products</span>
				</a>
			</li>
			<li>
				<a href="Adminbidding.php">
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">Manage Bidding</span>
				</a>
			</li>
			<li>
				<a href="Amembers.php">
					<i class='bx bxs-user-badge' ></i>
					<span class="text">Manage Members</span>
				</a>
			</li>
			<li>
				<a href="Ateam.php">
					<i class='bx bxs-group' ></i>
					<span class="text">Team</span>
				</a>
			</li>
		</ul>
		<ul class="side-menu">
		
			<li>
				<form action="Alogout.php" method="POST">
					<i class='bx bxs-log-out-circle' ></i>
              <button type="submit" name="logout" class="logout">Logout</button>
            </form>
			</li>
		</ul>
	</section>
	<!-- SIDEBAR -->
    <section id="content">
		<!-- NAVBAR -->
		<nav>
			<i class='bx bx-menu' ></i>
			<a href="#" class="nav-link">Categories</a>
			<form method="post" action="searchimgnew.php" id="searchform">
				<div class="form-input">
					<input type="text" name="Brand_Name" id="Brand_Name" placeholder="Search...">
					<button input type="submit" name="submit" class="search-btn"><i class='bx bx-search' ></i></button>
				</div>
			</form>
        </nav>
    </section>    
	<!-- CONTENT -->
	<section id="content">
		
		<!-- MAIN -->
		<main>
			<div class="head-title">
				<div class="left">
					<h1>Product Management</h1>
					
				</div>
				
			</div>

			<ul class="box-info">
				<li>
					<i class='bx bxs-calendar-check' ></i>
					<span class="text">
						<h3>1020</h3>
						<p>New Order</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-group' ></i>
					<span class="text">
						<h3>2834</h3>
						<p>Visitors</p>
					</span>
				</li>
				<li>
					<i class='bx bxs-dollar-circle' ></i>
					<span class="text">
						<h3>$2543</h3>
						<p>Total Sales</p>
					</span>
				</li>
			</ul>
		</main>
<?php
include_once 'Connection.php';
$result = mysqli_query($conn,"SELECT * FROM Vehicle");
?>
	<h1><center>Manager</center></h1>
	
<?php
if (mysqli_num_rows($result) > 0) {
?>
<div class="table-data">
	<table ><thead> 
	  <tr>
		<td> Id</td>
		<td>Brand Name </td>
		<td>Model</td> 
		<td>Category </td>
		<td>Milage</td>
		<td>User</td>
		<td>Price</td>
		<td>Transmission</td> 
		<td>Fuel Type</td> 
		<td>Image</td> 
		<td>Update Action</td>  
		<td>Delete Action</td> 
	  </tr>
	  </thead>
			<?php
			$i=0;
			while($row = mysqli_fetch_array($result)) {
			?>
	<tbody>
	<tr>
		<td><?php echo $row["Id"]; ?></td>  
		<td><?php echo $row["Brand_Name"]; ?></td>
		<td><?php echo $row["Model"]; ?></td> 
		  <td><?php echo $row["Category"]; ?></td>
		<td><?php echo $row["Milage"]; ?></td>
		<td><?php echo $row["User"]; ?></td> 
		  <td><?php echo $row["Price"]; ?></td> 
		<td><?php echo $row["Transmission"]; ?></td>
		<td><?php echo $row["FuelType"]; ?></td>
		<td>  <?php
 echo "<img src='".$row['Vehicle_Image']."' />"?></td>
		<td><a href="updatenew.php?Id=<?php echo $row["Id"]; ?>">Edit</a></td>
	    <td><a href="Delete.php?del=<?php echo $row['Id'];?>">Delete</a></td>	
        </tr>
	</tbody>

			<?php
			$i++;
			}
			?>
</table>
 <?php
}
else
{
    echo "No result found";
}
?>
</div>

	
	<script src="script.js"></script>
</body>
</html>