<?php
include('authentication.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<link rel="stylesheet" href="bids.css">
</head>

<body>
	<?php
include_once 'Connection.php';
$result = mysqli_query($conn,"SELECT * FROM vehiclebid");
?>
	
<?php
if (mysqli_num_rows($result) > 0) {
?>
<h1>Active Bidding Vehicles</h1>
<table ><thead><?php
	echo '<p><a href="javascript:history.go (-1)" title="Return to previous page">« Go back</a></p>';?>
	  <tr>
		 <td> Id</td>
		<td>Brand Name </td>
		<td>Model</td> 
		<td>Manufacture year </td>
		<td>Milage</td>
		<td>Owner</td>
		<td>Country of Manufacturer</td>
		<td>Transmission</td> 
		<td>Regular Priceprice</td> 
		  <td>Bid Start Amount</td> 
		<td>Bid End Date</td>
		<td>Image</td> 
		<td>Update Action</td>  
		<td>Delete Action</td> 
	  </tr>
	  </thead>
			<?php
			$i=0;
			while($row = mysqli_fetch_array($result)) {
			?>
	 <tbody> 
	  <tr>
		<td><?php echo $row["bidid"]; ?></td>  
		<td><?php echo $row["brandname"]; ?></td>
		<td><?php echo $row["model"]; ?></td> 
		  <td><?php echo $row["manyear"]; ?></td>
		<td><?php echo $row["milage"]; ?></td>
		<td><?php echo $row["owner"]; ?></td> 
		  <td><?php echo $row["CountryofMan"]; ?></td> 
		<td><?php echo $row["transmission"]; ?></td>
		<td><?php echo $row["regprice"]; ?></td>
		  <td><?php echo $row["bidstartamt"]; ?></td>
		<td><?php echo $row["bidenddate"]; ?></td>
		<td>  <?php
 echo "<img src='".$row['Bid_image']."' />"?></td>
		<td><a href="updatebid.php?bidid=<?php echo $row["bidid"]; ?>">Edit</a></td>
	    <td><a href="Deletebid.php?del=<?php echo $row['bidid'];?>">Delete</a></td>	
        </tr>
		</tbody>
			<?php
			$i++;
			}
			?>
</table>
 <?php
}
else
{
    echo "No result found";
}
?>
</body>
</html>